POS app using Django Rest Framework and Angularjs
=================================================

### How to run the app

+   `git clone git@bitbucket.org:aslamhadi/posdjangoangular.git`
+   Make sure you have installed virtualenv, pip, and bowel.
+   Set up virtualenv for this project
+   Run these commands:
	+	For OSX, run this command first: `brew install python cairo pango gdk-pixbuf libxml2 libxslt libffi`
    +   Install requirements :  `pip install -r requirements.txt`
    +   Install bower dependencies: `cd pos_app/front_end/static/` and `bower install`
+   Back to the root folder and run the server `./manage.py runserver`

I got the template from here : [http://bootstrapzero.com/bootstrap-template/dashgum](http://bootstrapzero.com/bootstrap-template/dashgum)